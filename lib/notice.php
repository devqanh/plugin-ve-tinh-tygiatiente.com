<?php
// notice_shortcode_huongdan
function notice_shortcode_huongdan() {
$screen = get_current_screen();

if($screen->id =='edit-post') :
    ?>
    <div class="updated fade">
        <p><?php _e('Shortcode get bài viết liên quan từ 2 website về [related_post id="1"] và [related_post id="2"]', 'sample-text-domain'); ?></p>
    </div>

<?php
endif;
}
add_action( 'admin_notices', 'notice_shortcode_huongdan' );