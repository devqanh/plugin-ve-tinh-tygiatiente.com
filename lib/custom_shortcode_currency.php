<?php
add_shortcode('currency_converter_calculator_qa', 'currency_converter_calculator_function');
function currency_converter_calculator_function()
{
    if (is_single()) :
        global $post;
        $tiente = get_field('tien_te', get_the_ID());
        $tiente= trim(explode('-', $tiente)[0]);
    endif;
    $shortcode = "<div class='ty-gia' style='height: 300px'>";
    if (!empty($tiente)) :
        $shortcode .= do_shortcode('[ccc_currency_converter_calculator lg="en" tz="7" fm="'.$tiente.'" to="VND" st="info" bg="FFFFFF" lr="" rd="" height="300"][/ccc_currency_converter_calculator]');
    else:
        $shortcode .= do_shortcode('[ccc_currency_converter_calculator lg="en" tz="7" fm="USD" to="VND" st="info" bg="FFFFFF" lr="" rd="" height="300"][/ccc_currency_converter_calculator]');
    endif;
    $shortcode .= "</div>";
    $shortcode .= "<style>
    .ty-gia iframe {
    height: 300px;
    }
    </style>";
    return $shortcode;


}

    // widget custom currency
class Quyenanh_Widget extends WP_Widget {

    /**
     * Thiết lập widget: đặt tên, base ID
     */
    function __construct() {
        $tpwidget_options = array(
            'classname' => 'Quyenanh_Widget_class', //ID của widget
            'description' => 'Mô tả của widget'
        );
        $this->WP_Widget('Quyenanh_Widget_id', 'QA currency Widget', $tpwidget_options);
    }


    /**
     * Show widget
     */

    function widget( $args, $instance ) {

        extract( $args );
        $title = apply_filters( 'widget_title', 'Chuyển Đổi Tiền Tệ '.date("d/m/Y") );

        echo $before_widget;

        //In tiêu đề widget
        echo $before_title.$title.$after_title;

        // Nội dung trong widget

        echo do_shortcode('[currency_converter_calculator_qa]');

        // Kết thúc nội dung trong widget

        echo $after_widget;
    }

}


add_action( 'widgets_init', 'create_Quyenanh_Widget' );
function create_Quyenanh_Widget() {
    register_widget('Quyenanh_Widget');
}
add_filter('deprecated_constructor_trigger_error', '__return_false');