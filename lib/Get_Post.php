<?php
/**
 * Created by PhpStorm.
 * User: Quyenanh
 * Date: 12/08/2018
 * Time: 3:07 SA
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

Class Api_cache
{
    protected $url_api; // url api
    protected $param;
    protected $trans;
    protected $time_cache;

    public function set_remote_api()
    {


        $api = "https://".$this->url_api . '/wp-json/wp/v2/' . $this->param;

        $list = wp_remote_get($api);
        $list = json_decode($list['body'], true);

        if (!isset($list->errors)) :
            $data = $list;
        else :
            $data = '';
        endif;

        return $data;

    }


}


Class Get_Post extends Api_cache
{
    private $slug_post;



    public function __construct()
    {
        $this->time_cache = 86400;

        add_shortcode('related', array($this, 'related_post'));
    }
    // set cache post
    public function set_cache_post_array($site,$id_post)
    {

        $post = $this->get_post_by_tag_id();

        if(!empty($post) && empty($post['data']['status'])   ) :
            foreach ($post as $posts) :
                $kq[] = array('title' => $posts['title']['rendered'], 'permalink' => $posts['link'], 'url_image' => $this->get_url_image_by_id($posts['featured_media']));
            endforeach;

            set_transient('set_cache_post_array_'.$site.'_'.$id_post, $kq, $this->time_cache);

        else:
            $kq = '';
        endif;

        return $kq;


    }
    //get cache post
    public function get_cache_post_array($site,$id_post)
    {
        $data = get_transient('set_cache_post_array_'.$site.'_'.$id_post); //set name cache
        if (!$data) :
            $data = $this->set_cache_post_array($site,$id_post);

        endif;

        return $data;
    }


    public function related_post($atts)
    {

        global $posts;
        $url=site_url();
        $url_array=$this->get_array_url($url);



        if (!empty(get_the_tags()[0]->slug)) :
            $this->slug_post = get_the_tags()[0]->slug;
        endif;
        if ($atts['id'] == 1) :
            $site=  $url_array[0];
            $this->url_api = $url_array[0];
        elseif ($atts['id'] == 2):
            $site = $url_array[1];
            $this->url_api = $url_array[1];
        endif;


        return $this->get_template_post($this->get_cache_post_array($site,get_the_ID()));



    }


    ///get url
    ///

    public function get_array_url($url)
    {
        $url_ex=explode('//',$url);
        $url=end($url_ex);
        $arg = array('kinhnghiemdidulich.com','thongtinsanbay.com', 'tygiatien.com');
        foreach ($arg as $value):
            if ($value != $url):
                $url_array[]=$value;
            endif;
        endforeach;
        return $url_array;
    }

    public function get_id_tag()
    {

        $this->param = "tags?slug={$this->slug_post}";
        if(!empty($this->set_remote_api())) :
            $data = $this->set_remote_api()[0]['id'];
        else :
            $data = '';
        endif;
        return $data ;
    }

    public function get_post_by_tag_id()
    {
        if(!empty($this->get_id_tag()) ):
        $this->param = "posts?tags={$this->get_id_tag()}&per_page=3";
        $data = $this->set_remote_api() ;
        else :
            $data = '';
        endif;
        return $data;
    }

    public function get_url_image_by_id($id_image)
    {
        $this->param = "media/{$id_image}";

        if(!empty($this->set_remote_api()['media_details']['sizes']['thumbnail']['source_url'] )) :
            $data = $this->set_remote_api()['media_details']['sizes']['thumbnail']['source_url']  ;
        else :
            $data ='';
        endif;
        return $data;
    }

    public function get_template_post($arr_post)
    {


        $posts = $arr_post;

        $html = '';
        if(!empty($posts)) :
            $html .= '        <div class="jnews_inline_related_post">
            <div class="jeg_postblock_20 jeg_postblock jeg_module_hook jeg_pagination_nextprev jeg_col_6o3 jnews_module_48_0_5b6a984e92b45 "
                 data-unique="jnews_module_48_0_5b6a984e92b45">
                <div class="jeg_block_heading jeg_block_heading_5 jeg_subcat_right">
                    <h3 class="jeg_block_title"><span>Bài viết liên quan</span></h3>
                </div>
                <div class="jeg_block_container">
                    <div class="jeg_posts jeg_load_more_flag">';
            foreach ($posts as $posts) :
                $permalink = $posts['permalink'];
                $url_image = $posts['url_image'];
                $title = $posts['title'];
                $html .= '<article
                                class="jeg_post jeg_pl_sm post-30 post type-post status-publish format-standard has-post-thumbnail hentry ">
                            <div class="jeg_thumb">
                                <a href="' . $permalink . '">
                                    <div class="thumbnail-container animate-lazy size-715 ">
                                    <img width="120" height="86" src="' . $url_image . '" class="attachment-jnews-120x86 size-jnews-120x86 wp-post-image lazyautosizes lazyloaded " alt=""  data-sizes="auto" data-expand="700" data-pin-no-hover="true" sizes="90px">
                                    </div>
                                </a>
                            </div>
                            <div class="jeg_postblock_content">
                                <h3 class="jeg_post_title"><a href="' . $permalink . '">' . $title . '</a></h3>
                                <div class="jeg_post_review jeg_review_stars jeg_landing_review">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </article>

         ';
            endforeach;
            $html .= '               </div>
                </div>
            </div>
        </div>';
        endif;
        return $html;
    }


}

