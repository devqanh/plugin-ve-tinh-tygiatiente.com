<?php

class Qa_Dom
{
    private $url;
    private $dom;
    private $get_dom;
    private $offset = 86400;
    private $data;

    public function __construct()
    {

        add_action('wp_enqueue_scripts', array($this, 'lichbay_style'), 3);
        add_shortcode('lich_bay', array($this, 'lich_bay'));
    }

//lib dom
    function simple_dom_qa()
    {
        require_once('simple_html_dom.php');
        $context = stream_context_create(array(
            'http' => array(
                'header' => array(
                    'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201'
                )
            )
        ));
        $html = file_get_html($this->url, false, $context);
        if (is_object($html)):
            $data = $html->find($this->dom, 0);
            $getdom = $this->get_dom;
            $data = $data->$getdom;
            $data = str_replace('Flight Schedules', 'Lịch trình bay', $data);
            $this->data = $data;
            $data = $this->cache_file_dom();
        else :
            $data = "Không có dữ liệu";
        endif;
        return $data;
    }


//cache file
    public function cache_file_dom()
    {

        $folder_cache = QA_DIR . "cache";
        if (!file_exists($folder_cache)) {
            mkdir($folder_cache, 0777, true);
        }
        $file = $folder_cache . '/file_cache_' . md5($this->url);
        if (file_exists($file) && fileatime($file) > time() - $this->offset) {
            $data = file_get_contents($file);
        } else {
            $data = $this->data;
            file_put_contents($file, $this->data);
        }

        return $data;
    }

    //funtion shortcode lich_bay
    public function lich_bay($args)
    {
        global $post;
        if (get_field('link_lich_trinh_bay')) :
            wp_enqueue_style('style_lichbay');
            wp_enqueue_script('js_lichbay');
            $this->url = get_field('link_lich_trinh_bay');
            $this->dom = '.js-schedules';
            $this->get_dom = 'outertext';
            $html = $this->simple_dom_qa();
        else :
            $html = '';
        endif;
        return $html;
    }

    //style shortcode lich_bay


    function lichbay_style()
    {
        wp_register_style('style_lichbay', QA_URL . '/assets/style.css');

        wp_register_script('js_lichbay', QA_URL . '/assets/sorter.js');

    }

}