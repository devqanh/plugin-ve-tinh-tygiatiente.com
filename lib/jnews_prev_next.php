<?php
add_action('jnews_single_post_after_content', 'next_prev_content_hook_custom', 20);
function next_prev_content_hook_custom() {
    $id_tag = get_the_tags(get_the_ID())[0]->term_id;
    $id_post = get_the_ID();
    $html = '<div class="jnews_prev_next_container"><div class="jeg_prevnext_post">';
    $args = array(
        'post_type' => 'post' ,
        'order' => 'ASC' ,
        'posts_per_page' => 1,
        'tag_id'=> $id_tag,
        'post__not_in'=>array($id_post)
    );
    $query = new WP_Query( $args );
    while ( $query->have_posts() ) : $query->the_post();
    $html .= '
            <a href="'.get_the_permalink().'" class="post prev-post">
            <span class="caption">Bài viết trước</span>
            <h3 class="post-title">'.get_the_title().'</h3>
            </a>
              ';
    endwhile;
    wp_reset_query();
    $args = array(
        'post_type' => 'post' ,
        'order' => 'ASC' ,
        'posts_per_page' => 1,
        'tag_id'=> $id_tag,
        'offset'=>1,
        'post__not_in'=>array($id_post)
    );
    $query = new WP_Query( $args );
    while ( $query->have_posts() ) : $query->the_post();
        $html .= '
            <a href="'.get_the_permalink().'" class="post next-post">
            <span class="caption">Bài viết sau</span>
            <h3 class="post-title">'.get_the_title().'</h3>
            </a>
              ';
    endwhile;
    wp_reset_query();


    $html .= '</div></div>';
    echo $html;
}